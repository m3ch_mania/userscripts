// ==UserScript==
// @name         MangaDex External Lookup Utility
// @namespace    https://gitlab.com/m3ch_mania/mangadex-externallookup
// @version      0.4302
// @icon         https://mangadex.org/favicon.ico
// @description  Adds a button to manga-edit pages that will attempt to populate external reference fields by performing search calls to the respective sites' APIs
// @author       m3ch_mania
// @updateURL    https://gitlab.com/m3ch_mania/mangadex-externallookup/raw/master/mangadex-externallookup.user.js
// @downloadURL  https://gitlab.com/m3ch_mania/mangadex-externallookup/raw/master/mangadex-externallookup.user.js
// @include      https://mangadex.org/manga/*
// @include      https://*.mangadex.org/manga/*
// @grant        none
// ==/UserScript==

/**
 * Todo list:
 * - Add function to handle all lookup calls
 * - Alter 'melu_autoPopulate' to use aforementioned new function upon completion
 * - Split 'melu_appendAndPushEntryChanges' into:
 *      - A function to simultaneously receive all accepted lookup results (only supports one-at-a-time commits right now)
 *      - A function to simultaneously push all accepted lookup results (only supports one-at-a-time commits right now)
 */

(function() {

    function melu_addButton() {
        // Assemble Auto Populate Button
        const autoPopulateButton = document.createElement('button')
        autoPopulateButton.classList.add('btn', 'btn-info', 'pull-right')
        autoPopulateButton.setAttribute('id', 'auto_populate_button')
        // Obtain Auto Populate insertion context
        // Note: Moved to main page instead of edit page to experiement with UX
        // const mdref_addLinkButton = document.getElementById('add_link_button')
        const editButton = document.getElementById('edit_button')
        // Add child elements to Auto Populate Button
        const buttonChildSpan = document.createElement('span')
        const buttonChildText = document.createTextNode(' Auto populate')
        buttonChildSpan.classList.add('fas', 'fa-search', 'fa-fw')
        buttonChildSpan.setAttribute('aria-hidden', 'true')
        autoPopulateButton.appendChild(buttonChildSpan)
        // Add listener for Auto Populate Button
        autoPopulateButton.addEventListener('click', function(event) {
            melu_autoPopulate()
        })
        // Attach Auto Populate Button adjacent to 'Edit' Button
        melu_insertAfter(autoPopulateButton, editButton)
        melu_insertAfter(buttonChildText, buttonChildSpan)
    }

    function melu_insertAfter(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling)
    }

    function melu_autoPopulate() {
        // Extract Manga Title for lookup
        const mangaTitle = document.getElementById('manga_name').value
        console.info('MELU: Core - Got request to perform external lookup')
        // Disable Auto Populate Button while we process the request
        const autoPopulateButtonInnerHtml = '<span class="fas fa-spinner fa-pulse fa-fw" aria-hidden="true" title=""></span> Searching...'
        const autoPopulateButton = document.getElementById('auto_populate_button')
        autoPopulateButton.innerHTML = autoPopulateButtonInnerHtml
        autoPopulateButton.disabled = true
        // Perform a lookup against Kitsu.io's database using the extracted title
        melu_lookupKitsuEntry(mangaTitle)
    }

    function melu_lookupKitsuEntry(mangaTitle) {
        const kitsuApiPath = 'https://kitsu.io/api/edge/manga?'

        fetch(kitsuApiPath, {
            'filter[subtype]': 'manga',
            'filter[text]': mangaTitle
        })
        .then(function(response) {
            if(response.ok) {
                return response.json()
            }
            throw new Error('Error occurred while interfacing with API')
        })
        .then(function(jsonData) {
            // Extract the first item in the response
            kitsuApiFirstResponse = jsonData.data[0]
            console.info(`MELU: Kitsu API - First Query Result: ${kitsuApiFirstResponse.attributes.titles.en_jp}`)
            // Prepare to link the first response to the MangaDex entry
            melu_commitKitsuEntry(kitsuApiFirstResponse)
        })
        .catch(function(error){
            console.error(`MELU: Kitsu API - Error: ${error.message}`)
            // Reenable the Auto Populate Button since the request failed
            const autoPopulateButtonInnerHtml = '<span class="fas fa-search fa-fw" aria-hidden="true" title=""></span> Auto Populate'
            const autoPopulateButton = document.getElementById('auto_populate_button')
            autoPopulateButton.innerHTML = autoPopulateButtonInnerHtml
            autoPopulateButton.disabled = false
            // Notify the user that an error occurred
            const commitMessageError = '<div class="alert alert-danger text-center" role="alert"><strong><span class="fas fa-times-circle fa-fw" aria-hidden="true" title="Error"></span> Error:</strong> There was a problem while interfacing with the Kitsu.io API.</div>'
            const messageContainer = document.getElementById('message_container')
            messageContainer.innerHTML = commitMessageError
            messageContainer.style.display = 'block'
            window.setTimeout(function() {
                melu_fadeOutEffect(messageContainer, 50)
            }, 3000)
            melu_fadeOutEffect(messageContainer, 50)
        })
    }

    function melu_commitKitsuEntry(kitsuApiFirstResponse) {
        // Test for alternate titles and return 'none' if null
        const kitsuApiResponseAltTile = (kitsuApiFirstResponse.attributes.abbreviatedTitles ? kitsuApiFirstResponse.attributes.abbreviatedTitles[kitsuApiFirstResponse.attributes.abbreviatedTitles.length -1] : 'none')
        const confirmText = 
        `Kitsu.io API Query Result:
        Title: ${kitsuApiFirstResponse.attributes.titles.en_jp}
        Alt Title: ${kitsuApiResponseAltTile}
        
        Click 'OK' to use this result, or 'Cancel' to discard it`
        console.info('MELU: Kitsu API - Prompting user for query result validation')
        // Prompt the user to use the query result
        confirmLinkResult = confirm(confirmText)
        if (confirmLinkResult) { // User accepted the query result
            console.info('MELU: Kitsu API - User accepted the query result')
            // Compile the form data, append the Kitsu entry ID, then submit the data
            melu_appendAndPushEntryChanges('nu', kitsuApiFirstResponse.attributes.slug)
        } else { // User rejected the query result
            console.warn('MELU: Kitsu API - User rejected the query result')
            // Reenable the Auto Populate Button since the user rejected the query result
            const autoPopulateButtonInnerHtml = '<span class="fas fa-search fa-fw" aria-hidden="true" title=""></span> Auto Populate'
            const autoPopulateButton = document.getElementById('auto_populate_button')
            autoPopulateButton.innerHTML = autoPopulateButtonInnerHtml
            autoPopulateButton.disabled = false
        }
    }

    function melu_appendAndPushEntryChanges(linkType, linkValue) {
        const mangadexAjaxPath = '/ajax/actions.ajax.php?function=manga_edit&id='
        const mangaEditForm = document.getElementById('manga_edit_form')
        // Extract the MangaDex Manga Entry ID from the current URI
        const mangaEntryId = document.location.href.match(/\/manga\/(\d{1,})\/?/)[1]
        // Create a new FormData object using the data from the edit form
        const customFormData = new FormData(mangaEditForm)
        console.info(`MELU: Core - Will commit entry '${mangaEntryId}' with new link of type '${linkType}' and value '${linkValue}'`)
        // Append the new link's type (site)
        customFormData.append('link_type[]', linkType)
        // Append the new link's value (slug/id/url)
        customFormData.append('link_id[]', linkValue)
        // Submit the appended form data using an AJAX call

        fetch(`${mangadexAjaxPath}${mangaEntryId}`, {
            body: customFormData,
            cache: 'no-cache',
            credentials: 'include',
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            method: 'POST'
        })
        .then(function(response) {
            if(response.ok) {
                console.info(`MELU: Core - Entry was committed`)
                const commitMessageSuccess = '<div class="alert alert-success text-center" role="alert"><strong><span class="fas fa-check-circle fa-fw" aria-hidden="true" title="Success"></span> Success:</strong> This title has been edited.</div>'
                const messageContainer = document.getElementById('message_container')
                messageContainer.innerHTML = commitMessageSuccess
                messageContainer.style.display = 'block'
                window.setTimeout(function() {
                    melu_fadeOutEffect(messageContainer, 50, `/manga/${mangaEntryId}`)
                }, 3000)
            } else {
                throw new Error(response.text())
            }
        })
        .catch(function(error){
            console.error(`MELU: Core - Error: ${error.message}`)
            // Reenable the Auto Populate Button since the request failed
            const autoPopulateButtonInnerHtml = '<span class="fas fa-search fa-fw" aria-hidden="true" title=""></span> Auto Populate'
            const autoPopulateButton = document.getElementById('auto_populate_button')
            autoPopulateButton.innerHTML = autoPopulateButtonInnerHtml
            autoPopulateButton.disabled = false
            // Notify the user that an error occurred
            const messageContainer = document.getElementById('message_container')
            messageContainer.innerHTML = error.message
            messageContainer.style.display = 'block'
            window.setTimeout(function() {
                melu_fadeOutEffect(messageContainer, 50)
            }, 3000)
        })
    }

    function melu_fadeOutEffect(fadeTarget, fadeDelay, redirect = false) {
        const fadeEffect = setInterval(function() {
            if(!fadeTarget.style.opacity) {
                fadeTarget.style.opacity = 1
            }
            if (fadeTarget.style.opacity > 0) {
                fadeTarget.style.opacity -= 0.1
            } else {
                clearInterval(fadeEffect)
                fadeTarget.innerHTML = null
                fadeTarget.style.opacity = 1
                fadeTarget.style.display = 'none'
                if (redirect) {
                    location.href = redirect
                }
            }
        }, fadeDelay)
    }

    // init
    melu_addButton()

})();
