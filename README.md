Collection of userscripts
======

MangaDex External Lookup Utility (MELU)
------

**__(Note: Currently a Proof-of-Concept Project)__**

This userscript assists MangaDex users in automatically populating reference links to third party databases.

When installed, an "Auto Populate" button is added to the manga entry page, just to the left of the edit button.

Clicking this button will automatically perform an API call to the third party sites supported by MangaDex.

If an appropriate entry is found, the user will be prompted to accept or reject the proposed reference entry.

Rejecting the proposed entry will do nothing. Accepting the proposed entry will automatically populate the relevant fields and submit the changes.

---

Support matrix:

| External Site | Status      | Note         |
| ------------- | ----------- | ------------ |
| MangaUpdates  | Unsupported | No API       |
| MyAnimeList   | Unsupported | API Disabled |
| NovelUpdates  | Unsupported | No API       |
| Raw/Orignal   | Unsupported | No API       |
| Official      | Unsupported | No API       |
| CDJapan       | Unsupported | No API       |
| Amazon.co.jp  | Unsupported | TBD          |
| Kitsu.io      | PoC         | PoC          |
| AniList       | Planned     | Planned      |

---

The Kitsu lookup module is only partially functional, as MangaDex does not yet have a reference field for this site.

Attempting to use the plugin at this time will result in an API query against Kitsu's database. Although the query and results are functional, there is no canonical location for this result. Thus, for testing purposes, the result will be written to the NovelUpdates field.

---

Button location context:

![Reference image for button location][referenceImage]


[referenceImage]: https://i.imgur.com/hYy5cks.png